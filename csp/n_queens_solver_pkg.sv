// Author: Francois Cerisier
// Company: AEDVICES - www.aedvices.com
// (c) 2021
//--------------------------------------------------------
// Description: 
//    This code implements a solver of the N queens on a chessboard problem.
//    It uses SystemVerilog language constructs to solve this problem with constraints.
// Usage:
//    import n_queens_solver_pkg::*;
//    
//    chess_board cb = new();
//    cb.randomize();
//    cb.print_board();
//--------------------------------------------------------
// Licence:
/*
  The contents of this file are subject to the restrictions and limitations
  set forth in the CeCILL-B License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.cecill.info . Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.
*/


`include "n_queens_inc.svh"

package n_queens_solver_pkg;

  parameter int unsigned SIZE = `D_SIZE;
  parameter int unsigned NR_WIDTH = 32;

  typedef bit[NR_WIDTH-1:0] position_t;

  /// \class chess_board
  /// Constrained Random Class to generate a chess_board
  class chess_board;

    // array to host the queens
    rand position_t queens[SIZE];

    constraint n_queens_c {
      // constraint fo reach queen
      foreach (queens[yy]) {
        // Limite X and Y to SIZE-1
        queens[yy] < SIZE;

        foreach (queens[zz]) {
          // we cannot place 2 queens on the same column
          zz != yy -> queens[zz] != queens[yy];
          // we cannot place 2 queens on the same SW2NE diagonal
          zz != yy -> queens[zz] !=  queens[yy] - ( yy - zz ) ;
          // we cannot place 2 queens on the same SE2NW diagonal
          zz != yy -> queens[zz] !=  queens[yy] + ( yy - zz ) ;
        }
      }
    }

    // Print the chess board
    task print_board();
      $write("Chess Board:\n");
      foreach (queens[yy]) begin 
        for (int xx=0;xx<SIZE;xx++) begin 
          $write("%0s ", queens[yy] == xx ? "Q" : "." );
        end
        $write("\n");
      end
    endtask

  endclass
endpackage

