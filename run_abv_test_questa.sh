#!/bin/env csh
##  Author: Francois Cerisier
##  Company: AEDVICES - www.aedvices.com
##  (c) 2021
## --------------------------------------------------------
##  Description: 
##     Launch Xcelium to check the design using random tests
##     generated with the CSP solver
## --------------------------------------------------------
##  Licence:
## 
## The contents of this file are subject to the restrictions and limitations
## set forth in the CeCILL-B License (the "License");
## You may not use this file except in compliance with such restrictions and
## limitations. You may obtain instructions on how to receive a copy of the
## License at http://www.cecill.info . Software distributed by Contributors
## under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
## ANY KIND, either express or implied. See the License for the specific
## language governing rights and limitations under the License.
## 



vlog +acc \
    +define+D_SIZE=40 \
    +incdir+inc \
    csp/n_queens_solver_pkg.sv \
    dut/chessboard.sv \
    abv/n_queens_abv.sv \
    abv/n_queens_abv_bind.sv \
    tb/n_queens_chessboard_tb.sv \

vsim \
    -sv_seed random \
    n_queens_chessboard_tb \
    -do "add wave -position insertpoint sim:/n_queens_chessboard_tb/dut/* ; run -all"


