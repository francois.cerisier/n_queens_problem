// Author: Francois Cerisier
// Company: AEDVICES - www.aedvices.com
// (c) 2021
//--------------------------------------------------------
// Description: 
//    This code implements a synthesizable design which flags
//    whenever we have N queens on a chess board which are not
//    attacking each other.
//--------------------------------------------------------
// Licence:
/*
  The contents of this file are subject to the restrictions and limitations
  set forth in the CeCILL-B License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.cecill.info . Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.
*/

`include "n_queens_inc.svh"


module chessboard #(
  parameter int unsigned N = `D_SIZE
)
(
  input clock,reset,
  input  logic[31:0] queens_pos[N],
  output logic       ok
);

  
  logic col_ok;
  logic diag1_ok;
  logic diag2_ok;
  

  always @(posedge clock)
    if ( reset )
      ok <= 0;
    else 
      ok <= col_ok && diag1_ok && diag2_ok;


  logic[N-1:0][N-1:0] matrix;

  // Place the queens in regard to their position
  always @(*) begin 
    for (int ii = 0 ; ii<N ; ii++) begin 
      for (int jj = 0 ; jj<N ; jj++) begin 
        if ( queens_pos[ii] == jj ) 
          matrix[ii][jj] = 1;
        else
          matrix[ii][jj] = 0;
      end
    end
  end

  // Check if the queens are well placed
  always @(*) begin 
    col_ok = 1;
    diag1_ok = 1;
    diag2_ok = 1;
    for (int ii = 0 ; ii<N ; ii++) begin 
      for (int jj = 0 ; jj<N ; jj++) begin 
        if ( (ii != jj) ) begin 
          // check that we do not have two queens on the same column
          if ( queens_pos[ii] == queens_pos[jj] )
            col_ok = 0;

          // CHeck that we do not have two queens on the diagonal
          if ( queens_pos[jj] == queens_pos[ii] - ( ii - jj ) )
            diag1_ok = 0;
          if ( queens_pos[jj] == queens_pos[ii] + ( ii - jj ) )
            diag2_ok = 0;
        end
      end
    end
  end

endmodule
