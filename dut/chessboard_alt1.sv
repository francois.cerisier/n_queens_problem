// Author: Francois Cerisier
// Company: AEDVICES - www.aedvices.com
// (c) 2021
//--------------------------------------------------------
// Description: 
//    This code implements a synthesizable design which flags
//    whenever we have N queens on a chess board which are not
//    attacking each other.
//    This alternatives crawls through the entire matrix, instead of 
//    looking only at the inputs. This shows how to crawl through the 
//    diagonals.
// Limitations:
//     N shall be an even number
//--------------------------------------------------------
// Licence:
/*
  The contents of this file are subject to the restrictions and limitations
  set forth in the CeCILL-B License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.cecill.info . Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.
*/

`include "n_queens_inc.svh"


module chessboard #(
  parameter int unsigned N = `D_SIZE
)
(
  input clock,reset,
  input  logic[31:0] queens_pos[N],
  output logic       ok
);

  
  logic line_ok;
  logic col_ok;
  logic diag1_ok;
  logic diag2_ok;
  

  always @(posedge clock)
    if ( reset )
      ok <= 0;
    else 
      ok <= line_ok && col_ok && diag1_ok && diag2_ok;

  logic[31:0] nr_queens_lin[N];
  logic[31:0] nr_queens_col[N];
  logic[31:0] nr_queens_diag1[-N+1:N-1];
  logic[31:0] nr_queens_diag2[-N+1:N-1];

  logic[N-1:0][N-1:0] matrix;

  assume property( @(posedge clock) (N % 2 == 0) ) else
  $fatal("Expect N to be even");

  // Place the queens on the matrix
  always @(*) begin 
    for (int ii = 0 ; ii<N ; ii++) begin 
      for (int jj = 0 ; jj<N ; jj++) begin 
        if ( queens_pos[ii] == jj ) 
          matrix[ii][jj] = 1;
        else
          matrix[ii][jj] = 0;
      end
    end
  end

  // check each line
  always @(*) begin 
    line_ok = 1;
    for (int ii=0;ii<N;ii++) begin
      if ( nr_queens_lin[ii] != 1 )
        line_ok = 0;
    end
  end 

  // check each col
  always @(*) begin 
    col_ok = 1;
    for (int ii=0;ii<N;ii++) begin
      if ( nr_queens_col[ii] != 1 )
        col_ok = 0;
    end
  end 

  // check diagonal 1
  always @(*) begin 
    diag1_ok = 1;
    for (int ii = 0 ; ii<2*N-1 ; ii++) begin 
      static int kk;
      kk = ii-N+1;
      if ( nr_queens_diag1[kk] > 1 )
        diag1_ok = 0;
    end
  end 

  // check diagonal @
  always @(*) begin 
    diag2_ok = 1;
    for (int ii = 0 ; ii<2*N-1 ; ii++) begin 
      static int kk;
      kk = ii-N+1;
      if ( nr_queens_diag2[kk] > 1 )
        diag2_ok = 0;
    end
  end 

  // count nr of queens per lines and column
  always @(*) begin 
    for (int ii = 0 ; ii<N ; ii++) begin 
      nr_queens_lin[ii] = 0;
      nr_queens_col[ii] = 0;

      for (int jj = 0 ; jj<N ; jj++) begin 

        // cnt per col
        if ( matrix[jj][ii] == 1 )
          nr_queens_col[ii] += 1;

        // cnt per line
        if ( matrix[ii][jj] == 1 )
          nr_queens_lin[ii] += 1;

      end
    end
  end

  // cnt per diag1
  always @(*) begin 
    for (int ii = 0 ; ii<2*N-1 ; ii++) begin 
      static int kk;
      kk = ii-N+1;
      nr_queens_diag1[kk] = 0;

      for (int jj=0; jj<N; jj++) begin 

        if ( ( jj + kk >= 0 ) && ( jj + kk < N ) && 
            ( matrix[jj][jj+kk] == 1 ) )
            
            nr_queens_diag1[kk] = nr_queens_diag1[kk] + 1 ;

      end
    end
  end


  // cnt per diag1
  always @(*) begin 
    for (int ii = 0 ; ii<2*N-1 ; ii++) begin 
      static int kk;
      kk = ii-N+1;
      nr_queens_diag2[kk] = 0;

      for (int jj=0; jj<N; jj++) begin 

        if ( ( jj + kk >= 0 ) && ( jj + kk < N ) && 
            ( matrix[N-1-jj][jj+kk] == 1 ) )
            
            nr_queens_diag2[kk] = nr_queens_diag2[kk] + 1 ;

      end
    end
  end



endmodule
