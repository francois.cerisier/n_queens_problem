# n_queens_problem

## CeCILL-B License
The contents of this file are subject to the restrictions and limitations
set forth in the CeCILL-B License (the "License");

You may not use this file except in compliance with such restrictions and
limitations. You may obtain instructions on how to receive a copy of the
License at http://www.cecill.info . Software distributed by Contributors
under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
ANY KIND, either express or implied. See the License for the specific
language governing rights and limitations under the License.


## Getting started

The repository shows how to solve the N queens problem using SystemVerilog.
Use the run_*.sh scripts to launch the associated tools. 
Use the define D_SIZE to run with a chessboard of other dimensions.

The complete problem is well described on Wikipedia: https://en.wikipedia.org/wiki/Eight_queens_puzzle

Two design alternatives are provided:
  chessboard.sv      --> Checks the input position array.
  chessboard_alt1.sv --> Checks the actual placements on the matrix.


## Constraint Solving Programing (CSP) 

Demonstration of how to solve the problem by generating random solutions 
using constrained random generation.

### Related Files
  * csp/n_queens_solver_pkg.sv  : the main solver package
  * tb/n_queens_csv_test.sv    : a test using the solver package

### Run with Questa

  * ./run_csp_test_questa.sh
    
### Run with Xcelium

  * ./run_csp_test_xcelium.sh

## Assertion Based Verification

Demonstration of how to solve the problem using Property Checking.

### Related Files

  * dut/chessboard.sv
  * abv/n_queens_abv.sv

### Run with JasperGold

  * ./run_abv_jasper.sh

## Combined ABV + CSP

Shows how to very the DUT used for ABV using CSP

### Run with Questa

  * ./run_abv_test_questa.sh
    
### Run with Xcelium

  * ./run_abv_test_xcelium.sh


