// Author: Francois Cerisier
// Company: AEDVICES - www.aedvices.com
// (c) 2021
//--------------------------------------------------------
// Description: 
//    Bind the Assertions to the Design
//--------------------------------------------------------
// Licence:
/*
  The contents of this file are subject to the restrictions and limitations
  set forth in the CeCILL-B License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.cecill.info . Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.
*/


`include "n_queens_inc.svh"

bind chessboard n_queens_abv#(.N(`D_SIZE))   n_queens_abv_inst ( .* );