// Author: Francois Cerisier
// Company: AEDVICES - www.aedvices.com
// (c) 2021
//--------------------------------------------------------
// Description: 
//    This code implements the assertions that checks
//    that a solution exists to the N queens problem.
//--------------------------------------------------------
// Licence:
/*
  The contents of this file are subject to the restrictions and limitations
  set forth in the CeCILL-B License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.cecill.info . Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.
*/

module n_queens_abv #(
  parameter int unsigned N = 40
)
(
  input clock,reset,
  input  logic[31:0] queens_pos[N],
  output logic       ok
);


  default clocking cb @(posedge clock);
  endclocking 

  default disable iff ( reset );

  // Assumes that all queens are on the chessboard 
  generate 
    genvar ii;
    for (ii = 0 ; ii< N ; ii ++)
      assume property ( queens_pos[ii] < N );
  endgenerate


  // If we can prove we cover this flag, then we have at least one solution
  // to the N queens problem.
  asrt_ok : cover property ( ok );

endmodule