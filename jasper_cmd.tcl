

clear -all

set SIZE 40

set_elaborate_single_run_mode off
analyze -sv +define+D_SIZE=$SIZE +incdir+inc { dut/chessboard.sv }
analyze -sv +define+D_SIZE=$SIZE +incdir+inc { abv/n_queens_abv.sv }
analyze -sv +define+D_SIZE=$SIZE +incdir+inc { abv/n_queens_abv_bind.sv }

elaborate -bbox_a -top chessboard

clock clock 
reset reset 

prove -all 


