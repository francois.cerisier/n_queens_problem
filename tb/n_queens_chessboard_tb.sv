// Author: Francois Cerisier
// Company: AEDVICES - www.aedvices.com
// (c) 2021
//--------------------------------------------------------
// Description: 
//    This code generate and prints a chess board of NxN with N queens
//--------------------------------------------------------
// Licence:
/*
  The contents of this file are subject to the restrictions and limitations
  set forth in the CeCILL-B License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.cecill.info . Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.
*/
`include "n_queens_inc.svh"

module n_queens_chessboard_tb;
  import n_queens_solver_pkg::*;
  parameter N = `D_SIZE;


  logic clock,reset;
  logic[31:0] queens_pos[N];
  logic       ok;

  // The Design Under Test
  chessboard #(.N(N))  dut( .* );


  default clocking clkb @(posedge clock); endclocking

  initial begin
    clock = 0;
    forever begin 
      clock = ~clock;
      #20ns;
    end
  end


  // Check that either the queens are well placed, or we introduced an error on purpose
  asrt_chk_ok:
  assert property ( 
    !reset |=> 
    ok || (!ok && $past(err_gen)) ) 
  else $fatal("DUT Error");

  bit err_gen;

  chess_board board;
  initial begin 
    reset = 1;
    ##2;

    $display("Starting search %0dx%0d",SIZE,SIZE);

    repeat (100*N) begin 
      board = new();
      assert (board.randomize() ) begin 
        
        randcase 
          // 99% of valid cases, generated from the solver
          99 : foreach ( board.queens[ii] ) begin 
            queens_pos[ii] <= board.queens[ii];
            err_gen = 0;
          end

          // 1% of the time, we shift and roll back to create invalid chessboards
          1 : foreach ( board.queens[ii] ) begin 
            queens_pos[ii] <= ((board.queens[ii]>=SIZE-1) ? 0 : board.queens[ii]+1);
            err_gen = 1;
          end

        endcase
        
        ##1; // next cycle
        reset = 0; // ensure we are no longer in reset. The first cycle is in reset so that we don't have any error

        // print the generated solution
        board.print_board();
        $display("Chess board is %0dx%0d",SIZE,SIZE);

      end
      else begin

        // Not able to generate
        $error("Unable to find a solution");
      end
    end

    $display("==== END OF TEST ====");
    $finish;
  end

endmodule