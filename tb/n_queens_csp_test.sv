// Author: Francois Cerisier
// Company: AEDVICES - www.aedvices.com
// (c) 2021
//--------------------------------------------------------
// Description: 
//    This code generate and prints a chess board of NxN with N queens
//--------------------------------------------------------
// Licence:
/*
  The contents of this file are subject to the restrictions and limitations
  set forth in the CeCILL-B License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.cecill.info . Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.
*/
`include "n_queens_inc.svh"


program n_queens_csp_test;

  import n_queens_solver_pkg::*;

  chess_board cb = new();

  initial begin 

    $display("Starting search %0dx%0d",SIZE,SIZE);

    repeat (1) begin 

      assert (cb.randomize() ) begin 

        // print the solution
        cb.print_board();
        $display("Chess board is %0dx%0d",SIZE,SIZE);

      end
      else begin

        // Not able to generate
        $error("Unable to find a solution");
      end
    end

  end

endprogram